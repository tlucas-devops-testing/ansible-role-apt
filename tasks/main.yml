# Copyright (c) 2018-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# https://docs.ansible.com/ansible/latest/modules/include_tasks_module.html
- include_tasks:
    "{{ apt_tasks }}"
  with_first_found:
    - files:
        - "os/{{ ansible_distribution | lower }}/main.yml"
      skip: true
  loop_control:
    loop_var: "apt_tasks"

# https://docs.ansible.com/ansible/latest/modules/template_module.html
- name:
    "template : /etc/apt/apt.conf.d/custom-install-recommends"
  template:
    src: "templates/apt.conf.d/custom-install-recommends.j2"
    dest: "/etc/apt/apt.conf.d/custom-install-recommends"
    group: "root"
    owner: "root"
    mode: "0644"
  become:
    true

# https://docs.ansible.com/ansible/latest/modules/apt_key_module.html
- name:
    "apt_keys"
  apt_key:
    data: "{{ item.value.data | default(omit) }}"
    file: "{{ item.value.file | default(omit) }}"
    id: "{{ item.key }}"
    keyring: "/etc/apt/trusted.gpg.d/{{ item.value.keyring | mandatory }}.gpg"
    keyserver: "{{ item.value.keyserver | default(omit) }}"
    state: "{{ item.value.state | default(omit) }}"
    url: "{{ item.value.url | default(omit) }}"
    validate_certs: "{{ item.value.validate_certs | default(omit) }}"
  with_dict:
    "{{ apt_keys | default({}) }}"
  become:
    true

# https://docs.ansible.com/ansible/latest/modules/apt_repository_module.html
- name:
    "apt_repositories"
  apt_repository:
    codename: "{{ item.value.codename | default(omit) }}"
    filename: "{{ item.key }}"
    mode: "{{ item.value.mode | default(omit) }}"
    repo: "{{ item.value.repo | mandatory }}"
    state: "{{ item.value.state | default('present') }}"
    update_cache: "{{ item.value.update_cache | default(omit) }}"
    validate_certs: "{{ item.value.validate_certs | default(omit) }}"
  with_dict:
    "{{ apt_repositories | default({}) }}"
  become:
    true
